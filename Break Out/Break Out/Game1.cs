using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Break_Out
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D paddle, ball, brick;
        Vector2 paddlePos, ballPos;
        public static int winHeight = 600;
        public static int winWidth = 800;
        const float PADDLESPEED = 3.0f;
        const float BALLSPEED = 4.0f;
        List<Vector2> brickPos;
        List<bool> showBrick;
        Random random;
        int bounceAngle;
        bool start;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here           
            base.Initialize();
            paddlePos = new Vector2(winWidth / 2 - paddle.Width / 2, winHeight - paddle.Height);
            ballPos = new Vector2(winWidth / 2 - ball.Width / 2, paddlePos.Y - ball.Height);
            brickPos = new List<Vector2>();
            showBrick = new List<bool>();
            for (int i = 0; i < winWidth / brick.Width; i++ )
            {
                for (int j = 0; j < 4; j++)
                {
                    brickPos.Add(new Vector2(i * brick.Width, (j + 1) * brick.Height));
                    showBrick.Add(true);
                }
            }
            random = new Random();
            start = false;
            graphics.PreferredBackBufferWidth = winWidth;
            graphics.PreferredBackBufferHeight = winHeight;
            graphics.IsFullScreen = false;
            graphics.ApplyChanges();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            paddle = Content.Load<Texture2D>("Assets\\Paddle");
            ball = Content.Load<Texture2D>("Assets\\Ball");
            brick = Content.Load<Texture2D>("Assets\\Brick");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();
            
            if (Keyboard.GetState().IsKeyDown(Keys.Right) && paddlePos.X + paddle.Width < winWidth)
            {
                if (paddlePos.X + paddle.Width / 2 == ballPos.X + ball.Width / 2 && paddlePos.Y == ballPos.Y + ball.Height)
                    ballPos.X += PADDLESPEED;
                paddlePos.X += PADDLESPEED;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left) && paddlePos.X > 0)
            {
                if (paddlePos.X + paddle.Width / 2 == ballPos.X + ball.Width / 2 && paddlePos.Y == ballPos.Y + ball.Height)
                    ballPos.X -= PADDLESPEED;
                paddlePos.X -= PADDLESPEED;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Space) && paddlePos.X + paddle.Width / 2 == ballPos.X + ball.Width / 2 && paddlePos.Y == ballPos.Y + ball.Height)
            {
                bounceAngle = random.Next(165);
                while(bounceAngle < 15)
                    bounceAngle = random.Next(165);
                start = true;
            }
            if (start)
            {
                collide();
                ballPos.X += (float)Math.Round((BALLSPEED * Math.Cos(MathHelper.ToRadians(bounceAngle - 180))), 0);
                ballPos.Y += (float)Math.Round((BALLSPEED * Math.Sin(MathHelper.ToRadians(bounceAngle - 180))), 0);
                if (ballPos.X <= 0.0f || ballPos.X + ball.Width >= winWidth)
                    bounceAngle = 180 - bounceAngle;
                else if (ballPos.Y <= 0.0f || ballPos.Y + ball.Height >= paddlePos.Y && ballPos.X + ball.Width >= paddlePos.X + paddle.Width/4 && ballPos.X <= paddlePos.X + 3 * (paddle.Width/4))
                    bounceAngle = 0 - bounceAngle;
                else if (ballPos.Y + ball.Height >= paddlePos.Y && ballPos.X + ball.Width >= paddlePos.X && ballPos.X <= paddlePos.X + paddle.Width / 4)
                {
                    bounceAngle = random.Next(90);
                    while (bounceAngle < 15)
                        bounceAngle = random.Next(90);
                }
                else if (ballPos.Y + ball.Height >= paddlePos.Y && ballPos.X + ball.Width >= paddlePos.X + paddle.Width - paddle.Width / 4 && ballPos.X <= paddlePos.X)
                {
                    bounceAngle = random.Next(90);
                    while (bounceAngle < 15)
                        bounceAngle = random.Next(90);
                    bounceAngle = 180 - bounceAngle;
                }
                else if ((ballPos.Y + ball.Height >= paddlePos.Y && ballPos.Y + ball.Height >= winHeight) && (ballPos.X + ball.Width >= paddlePos.X && ballPos.X <= paddlePos.X + paddle.Width))
                    bounceAngle = -1 * (180 - bounceAngle);
                else if (ballPos.Y >= winHeight)
                {
                    start = false;
                    ballPos.X = paddlePos.X + paddle.Width / 2 - ball.Width / 2;
                    ballPos.Y = paddlePos.Y - ball.Height;
                }
            }

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        void collide()
        {
            for(int i = 0; i < brickPos.Count(); i++)
            {
                if (showBrick[i] == true && ballPos.Y + ball.Height >= brickPos[i].Y && ballPos.Y <= brickPos[i].Y + brick.Height && ballPos.X + ball.Width >= brickPos[i].X && ballPos.X <= brickPos[i].X + brick.Width)
                {
                    if (ballPos.Y + ball.Height >= brickPos[i].Y && ballPos.Y <= brickPos[i].Y + brick.Height)
                        bounceAngle = 0 - bounceAngle;
                    else
                        bounceAngle = 180 - bounceAngle;
                    showBrick[i] = false;
                }
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            spriteBatch.Draw(paddle, paddlePos, Color.White);

            for (int i = 0; i < brickPos.Count(); i++)
            {
                if (showBrick[i])
                {
                    if (brickPos[i].Y == 25.0f)
                        spriteBatch.Draw(brick, brickPos[i], Color.Red);
                    else if (brickPos[i].Y == 50.0f)
                        spriteBatch.Draw(brick, brickPos[i], Color.Green);
                    else if (brickPos[i].Y == 75.0f)
                        spriteBatch.Draw(brick, brickPos[i], Color.Blue);
                    else
                        spriteBatch.Draw(brick, brickPos[i], Color.Black);
                }
            }
            spriteBatch.Draw(ball, ballPos, Color.White);
            spriteBatch.End();
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
